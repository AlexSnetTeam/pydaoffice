# pydaoffice

## Introduction
### What is it pydao
*pydaoffice* is a python interface for [DaOffice.ru](http://daoffice.ru/) API.

### What is DaOffice.ru
*DaOffice.ru* is a corporate social network (like yammer).

### License
This code is open sourced under MIT license, GNU GPL 2 or later and GNU LGPL 2 or later. Please see the *license.txt* file for details.

### Authors
* Alexey Grechishkin aka Alex Snet( [mail](mailto:me@alexsnet.ru), [blog](http://alexsnet.ru) )

## Installation
### Automatically with PIP
	pip install git+git@bitbucket.org:AlexSnet/pydao.git

### Manualy
* Download source archive from [downloads page](https://bitbucket.org/AlexSnet/pydao/downloads).
* Extract archive (Example:  *tar -xzf master.tar.gz*)
* Chande dir to *pydaoffice* source root
* Execute *python setup.py install*

## Quickstart
For ordinary use:

	from pydaoffice import DaOffice
	office = DaOffice( PARAMETRS )
	resp = office.call('method.name')
	
For use with sugar:

	from pydaoffice import DaOfficeSugar
	office = DaOfficeSugar( PARAMETRS )
	resp = office.method.name()

## Advanced Usage


