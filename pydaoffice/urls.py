from django.conf.urls import patterns, url

urlpatterns = patterns('pydaoffice.views',
    url(r'^profile/(?P<user_id>\d+)$', 'get_dao_profile', name='dao_profile'),
    url(r'^profile/my$', 'get_my_dao_profile', name='dao_my_profile'),
    url(r'^users/all/(?P<page>\d+)$', 'get_dao_users', name='dao_users_all'),
    url(r'^followers/my$', 'get_my_followers', name='dao_followers_my'),
    url(r'^followers/(?P<user_id>\d+)$', 'get_followers', name='dao_followers'),
)