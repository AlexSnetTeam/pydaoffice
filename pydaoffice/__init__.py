from pydaoffice.pydao import DaOffice
from pydaoffice.sugar import DaOfficeSugar

VERSION = (12,9,26,1)
__version__ = '.'.join(map(str,VERSION))

__all__ = (
    'DaOffice',
    'DaOfficeSugar'
)