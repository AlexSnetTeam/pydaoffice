#
# Django Authorization backend
#

import datetime

from django.contrib.auth.models import User
from django.conf import settings
from django.contrib.auth.backends import ModelBackend

from pydaoffice import DaOffice
from pydaoffice import models as pydaomodels
from pydaoffice.signals import daoffice_user_registered

class DaofficeBackend(ModelBackend):
    """
    This is the DaOffice.ru Authentication Backend for Django
    """
    def authenticate(self, username=None, password=None):
        """
        Main authentication method

        !!! username is email !!!
        """
        email = username
        username = username.split('@')[0]

        self.url = getattr(settings,'DAOFFICE_URL',None)
        self.key = getattr(settings,'DAOFFICE_KEY',None)

        if not self.url:
            return None
        if not self.key:
            return None

        self.dao = DaOffice(self.url)
        try:
            self.dao.call('oauth.token',{
                'grant_type':       'password',
                'email':            email,
                'password':         password,
                'client_secret':    self.key
            })
        except Exception, e:

            return None

        user = self._find_existing_user(username, email)

        if True:
            if user:
                user.set_password(password)
                user.save()
                return user
            else:
                user = self._create_new_user_from_daoffice_response(username, password, self.dao.get_profile())
                user.save()
                return user
        else:
            return None

    def _find_existing_user(self, username, email):
        """
        Finds an existing user with provided username if one exists.
        Private service method.
        """
        users = User.objects.filter(username=username)
        if users.count() <= 0:
            users = User.objects.filter(email=email)
            if users.count() <= 0:
                return None
            else:
                return users[0]
        else:
            return users[0]


    def _create_new_user_from_daoffice_response(self, username, password, userdata):
        """
        Creating a new user in django auth database basing on information provided by DaOffice.
        Private service method.
        """
        user = User.objects.create_user(username, userdata.email, password)
        user.first_name, user.last_name = userdata.full_name.split(' ')
        user.is_active = True
        user.save()

        token, created = pydaomodels.DaoToken.objects.get_or_create(user=user)
        if not token.valid():
            token.expires = datetime.datetime.fromtimestamp(self.dao.connection['token']['expires'])
            token.token = self.dao.connection['token']['token']
            token.save()

#        profile, created = pydaomodels.DaoProfile.objects.get_or_create(user=user)
#        profile.dao_id = userdata.id
#        profile.full_name = userdata.full_name
#        profile.save()

#        daoffice_user_registered.send(sender=self, user=user, token=token, daoprofile=profile)

        return user
