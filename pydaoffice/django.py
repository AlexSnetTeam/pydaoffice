
from django.conf import settings

from pydaoffice import DaOffice, DaOfficeSugar

DAO_OBJECT_CACHE = None
def get_dao_object(sugar=False):
    if issubclass(DAO_OBJECT_CACHE, DaOffice):
        return DAO_OBJECT_CACHE

    url = getattr(settings,'DAOFFICE_URL',None)
    key = getattr(settings,'DAOFFICE_KEY',None)
    acc = getattr(settings,'DAOFFICE_ACC',None)
    pas = getattr(settings,'DAOFFICE_PAS',None)

    if not url:
        return None
    if not key:
        return None
    if not acc:
        return None
    if not pas:
        return None

    daoClass = DaOffice if not sugar else DaOfficeSugar
    dao = daoClass(url=url)
    try:
        resp = dao.call('oauth.token',
            {
                'grant_type':'password',
                'email':acc,
                'password':pas,
                'client_secret':key
            }
        )
    except Exception, e:
        resp = e

    DAO_OBJECT_CACHE = dao
    return DAO_OBJECT_CACHE