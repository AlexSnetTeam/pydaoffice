
import string
import time

from urlparse import urlparse

# Only one dependence
import requests # Python networks with human face

# Exceptions
from pydaoffice.exceptions import *

# DaOffice objects
from pydaoffice.objects import *

# In every time you can override default method
METHOD_OVERRIDES = {}

def WrapResponse(method=None, office=None, params=None, headers=None):
    """

    """
    def methodToClass(method):
        """
        Remove digits in method names
        """
        new_method = method.lower()
        for digit in string.digits:
            new_method = new_method.replace(digit,'')
        return new_method.strip('.')

    def classNameToMethod(name):
        name = name.replace('DaoMethod','')
        method = ''.join([ a if a not in string.ascii_uppercase else '.%s' % str(a).lower() for a in name ])
        method = method[1:] if str(method).startswith('.') else method
        return method

    def methodUp(method):
        new_method = method.lower()
        new_method = new_method.split('.')
        del new_method[-1]
        return '.'.join(new_method)

    methods = DaoMethodResponse.__subclasses__()
    methods = dict([ (classNameToMethod(m.__name__), m) for m in methods ])
    methods.update(METHOD_OVERRIDES)

    if not method:
        return methods

    new_method = methodToClass(method)
    while len(new_method)>=1:
        if new_method in methods:
            return methods[new_method](office=office, method=method, params=params, headers=headers)
        new_method = methodUp(new_method)

    return DaoMethodResponse(office=office, method=method, params=params, headers=headers)



class DaoMethodResponse(object):

    status = False

    response = None
    office = None

    error = None
    error_messages  = dict()

    method = None

    request_uri     = None
    request_headers = dict()
    request_params  = dict()

    _objects = list()
    _iter = 0

    def __init__(self, office=None, method=None, params=None, headers=None, dontcall=None):

        if office is None:
            raise DaoWrongOfficeObjectError(office)
        self.office = office

        if method is None:
            raise DaoParameterMisplaced('method')

        self.method = method
        self.request_uri = self._generate_api_link(method)

        self.request_headers = self._generate_request_headers()
        if headers is not None:
            self.request_headers.update(headers)

        self.request_params = dict()
        if params is not None:
            self.request_params.update(params)

        if dontcall is None:
            self.call()

    def __call__(self, *args, **kwargs):
        self.call(*args,**kwargs)

    def call(self, *args, **kwargs):
        self.request_params.update(kwargs)
        self._request()
        self._check_status()
        self._parse()

    def __iter__(self):
        while True:
            if self._iter > len(self._objects) and self.isMultipage:
                if self.request_params.has_key('page'):
                    self.request_params['page'] += 1
                else:
                    self.request_params['page'] = 1
                self._request()
                self._check_status()
                self._parse()
            try:
                o = self._objects[self._iter]
                self._iter += 1
            except IndexError:
                self._iter = 0
                raise StopIteration
            yield o

    def __len__(self):
        return len(self._objects)

    def __getitem__(self, item):
        return self._objects[item]

    def __repr__(self):
        """
        Displays the module, class and name of the field.
        """
        path = '%s.%s' % (self.__class__.__module__, self.__class__.__name__)
        name = getattr(self, 'name', None)
        if name is not None:
            return '<%s: %s>' % (path, name)
        return '<%s>' % path

    def _check_status(self):
        if not self.response.json:
            raise DaoWrongResponseObjectError(extra=self)

        if str(self.response.json['status']) == 'error':
            self.error = self.response.json['error']
            self.error_messages = self.response.json['error_messages']
            raise DaoResponseError(self.error, extra=self)

        if str(self.response.json['status']) == 'ok':
            self.status = True

    def _generate_request_headers(self):
        headers = dict()
        headers['Host'] = urlparse(self.office.connection['url']).hostname
        if self.office.connection['token']['token']:
            headers['Authorization'] = 'OAuth %s' % self.office.connection['token']['token']
        return headers

    def _generate_api_link(self, method):
        baseurl = self.office.connection['apiurl'] or self.office.connection['url']
        baseurl = (baseurl + '/') if not str(baseurl).endswith('/') else baseurl
        if type(method) in (types.ListType, types.TupleType,):
            method = '/'.join(method)
        if '.' in method:
            method = method.replace('.','/')
        if str(method).startswith('/'):
            method = method[1:]
        url = baseurl + method
        return url

    def _request(self):
        if self.request_params:
            self.response = requests.post(self.request_uri, data=self.request_params, headers=self.request_headers)
        else:
            self.response = requests.get(self.request_uri, headers=self.request_headers)

    def _parse(self):
        raise NotImplementedError

    @property
    def isMultipage(self):
        return True if dict(self.response.json).has_key('load_more_url') else False


class DaoMethodNetwork(DaoMethodResponse):
    """
    Getting all network credentials such as base_url, title, etc...
    """
    def _check_status(self):
        pass

    def _request(self):
        if self.request_uri.startswith('https://'):
            self.request_uri = self.request_uri.replace('https://', 'http://')

        self.response = requests.get(self.request_uri, headers=self.request_headers, allow_redirects=True)

    def _parse(self):
        if self.response.status_code is not 200:
            return
        if str(self.response.json['status']) is not 'ok':
            return

        self.office.connection['apiurl'] = self.response.json['network']['base_url']
        self.office.connection['title'] = self.response.json['network']['title']
        self.office.connection['gaids'] = self.response.json['network']['google_analytics_ids']

class DaoMethodOauthToken(DaoMethodResponse):
    def _parse(self):
        self.office.connection['token']['expires_in'] = self.response.json['token']['expires_in']
        self.office.connection['token']['token']      = self.response.json['token']['access_token']
        self.office.connection['token']['expires'] = int(time.time()) + int(self.office.connection['token']['expires_in'])
        self.office.connection['userid'] = self.response.json['token']['user_id']

class DaoMethodUsers(DaoMethodResponse):
    """
    Used for all API methods, that starts with users.
    """
    _page = 1
    _objects = list()
    def _parse(self):
        if 'follow' in self.method:
            return self.response.json['status'] is 'ok'
        else:
            if self.response.json.has_key('users'):
                for user in self.response.json['users']:
                    self._objects.append(DaoObjectUser(user,self.office))
            elif self.response.json.has_key('user'):
                self._objects.append(DaoObjectUser(self.response.json['user'], self.office))
            else:
                raise DaoResponseError


    def __getattr__(self, item):
        """
        """
        if 'by_id' in self.method and hasattr(self._objects[0], item):
            return getattr(self._objects[0], item)



class DaoMethodNotifications(DaoMethodResponse):
    """

    """
    def _parse(self):
        if self.response.status_code is not 200:
            return
        if str(self.response.json['status']) is not 'ok':
            return

class DaoMethodPrivatemessages(DaoMethodResponse):
    """

    """
    def _parse(self):
        if self.response.status_code is not 200:
            return
        if str(self.response.json['status']) is not 'ok':
            return

        if self.response.json.has_key('private_messages'):
            for message in self.response.json['private_messages']:
                self._objects.append(DaoObjectPrivateMessage(message, self))

class DaoMethodFiles(DaoMethodResponse):
    """

    """
    def _parse(self):
        if self.response.status_code is not 200:
            return
        if str(self.response.json['status']) is not 'ok':
            return

class DaoMethodSync(DaoMethodResponse):
    """

    """
    def _parse(self):
        if self.response.status_code is not 200:
            return
        if str(self.response.json['status']) is not 'ok':
            return


class DaoMethodSms(DaoMethodResponse):
    """

    """
    def _parse(self):
        if self.response.status_code is not 200:
            return
        if str(self.response.json['status']) is not 'ok':
            return

