"""
This is simply a wrapper for DaOffice class with some syntax-sugar.

With this wrapper you can call remote API methods just by nesting arguments an your code.
Like this:
    dao = DaOfficeSugar( ... )
    dao.network()
    all_users = dao.users.all()

"""
from pydaoffice.pydao import DaOffice

class DaOfficeSugar(DaOffice):
    # This is for method calling
    _method_path = list()

    #
    # Magic methods
    #
    def __getattr__(self, item):
        """
        Doing magic around method calling
        """
        if len(self._method_path) is 0:
            if item in ['trait_names', '_getAttributeNames', '__members__', '__methods__']:
                return getattr(self, item)

        self._method_path.append(item)
        return self

    def _rempath(self):
        """
        Removing path list
        """
        #del self._method_path
        self._method_path = list()

    def __del__(self):
        pass

    def __call__(self, *args, **kwargs):
        """
        Calling DaOffice API method
        """

        # Concatenating attributes to a string via '/'
        path =  '/'.join(self._method_path + [ str(a) for a in args] )

        self._rempath()
        return self.call(method=path,params=kwargs)