import types

from pydaoffice import exceptions


class DaoObjectInterface(object):
    _data = None

    def __init__(self, data=None, office=None):
        if office:
            self.office = office
        if type(data) is types.DictionaryType:
            self._data = data
            self._data_to_model()
        elif issubclass(data, DaoObjectInterface):
            self._convert(data)
        else:
            raise Exception('Unknown object type')

    def _convert(self, object):
        raise NotImplementedError

    def _data_to_model(self):
        for k in self._data:
            setattr(self, k, self._data[k])

    def __cmp__(self, other):
        from pydaoffice.methods import DaoMethodResponse
        if isinstance(other, self.__class__):
            return cmp(self._data, other._data)
        elif issubclass(other.__class__, DaoMethodResponse):
            return cmp(self._data, other._objects[0]._data) if len(other._objects)==1 else False
        else:
            return cmp(self._data, other)

    def __repr__(self):
        """
        Displays the module, class and name of the field.
        """
        #return force_str('<%s: %s>' % (self.__class__.__name__, u))
        path = '%s.%s' % (self.__class__.__module__, self.__class__.__name__)

        if self.__str__():
            return '<%s: %s>' % (path, self.__str__() )

        return '<%s>' % path

    def __getitem__(self, item):
        if self._data.has_key(item):
            return self._data[item]

    def __getattr__(self, item):
        if self._data.has_key(item):
            return self._data[item]

class DaoObjectUser(DaoObjectInterface):
    _cache = dict()

    def __str__(self):
        if self.id:
            return '#%d' % self.id

    def follow(self):
        if self.id is not self.office.connection['userid']:
            if self.office.call('users.follow.%i'%self.id):
                self.update()

    def unfollow(self):
        if self.id is not self.office.connection['userid']:
            if self.office.call('users.unfollow.%i'%self.id):
                self.update()

    def update(self):
        resp = self.office.call('users.by_id.%i'%self.id)
        self._data = resp.request.json['user']
        self._data_to_model()

    def getFullInfo(self):
        if not self.isFullInfo:
            resp = self.office.call('users.by_id.%i'%self.id)
            self._data = resp.request.json['user']
            self._data_to_model()

    @property
    def isFullInfo(self):
        return self._data.has_key('hobby')

    @property
    def isFollowed(self):
        if not self.office._user:
            self.office._user = self.office.call('users.by_id.%i'%self.office.connection['userid'])
        return self.office._user in self.following()

    @property
    def isFollower(self):
        if not self.office._user:
            self.office._user = self.office.call('users.by_id.%i'%self.office.connection['userid'])
        return self in self.office._user.followers()

    def isFollowerFor(self, user):
        from pydaoffice.methods import DaoMethodResponse
        if isinstance(user, self.__class__):
            user = user
        elif issubclass(user.__class__, DaoMethodResponse):
            user = user._objects[0]._data if len(user._objects)==1 else None
        else:
            user = self.office.call('users.by_id.%i'%user)._objects[0]
        return self in user.followers()

    def isFollowedBy(self, user):
        from pydaoffice.methods import DaoMethodResponse
        if isinstance(user, self.__class__):
            user = user
        elif issubclass(user.__class__, DaoMethodResponse):
            user = user._objects[0]._data if len(user._objects)==1 else None
        else:
            user = self.office.call('users.by_id.%i'%user)._objects[0]
        return self in user.following()

    def followers(self, force=False):
        if self._cache.has_key('followers') and not force:
            return self._cache['followers']

        self._cache['followers'] = list()

        resp = self.office.call('users.followers.%i'%self.id)
        for user in resp:
            self._cache['followers'].append(user)
        return self._cache['followers']

    def following(self, force=False):
        if self._cache.has_key('following') and not force:
            return self._cache['following']

        self._cache['following'] = list()

        resp = self.office.call('users.following.%i'%self.id)
        for user in resp:
            self._cache['following'].append(user)
        return self._cache['following']

    def sendMessage(self, text="", files=list()):
        resp = None
        try:
            resp = self.office.call('privatemessages.create',{'text':text, 'assign_to_user_ids':[self.id,], 'file_ids':files})
        except:
            pass
        return resp.status or False


class DaoObjectPrivateMessage(DaoObjectInterface):
    pass

class DaoObjectNotification(DaoObjectInterface):
    pass