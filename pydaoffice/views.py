from django.http import HttpResponse
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _, ugettext as __
from django.contrib.auth.models import User
from django.conf import settings
from django.core import serializers

from utils import request_is_xhr, response_json

from pydaoffice import DaOffice
from pydaoffice.models import DaoProfile, DaoToken

try:
    import simplejson as json
except ImportError:
    import json

def get_DaOffice_by_token(token=None):
    if not token:
        return None
    url = getattr(settings,'DAOFFICE_URL',None)
    key = getattr(settings,'DAOFFICE_KEY',None)
    if not url:
        return None
    if not key:
        return None
    return DaOffice(url=url, token=token, secret=key)

def get_DaOffice_for_current_user(request):

    try:
        dao_token = DaoToken.objects.get(user=request.user)
    except:
        return response_json({
            'result' : False,
            'message': __('User doesn\'t connected with DaOffice')
        })
    return get_DaOffice_by_token(dao_token.token if dao_token.valid else None)

@login_required
def get_dao_profile(request, user_id=None):
    if not user_id:
        return response_json({
            'result' : False,
            'message': __('No user specified')
        })

    dao = get_DaOffice_for_current_user(request)

    resp = dao.call('users.by_id.%i'%user_id)
    print resp.response.text

    return response_json({
        'result' : True,
        'profile' : {}
    })

@login_required
def get_my_dao_profile(request):
    if not request.user:
        return response_json({
            'result' : False,
            'message': __('No user specified')
        })

    return get_dao_profile(request=request, user_id=request.user.id)

@login_required
def get_dao_users(request, page=1):
    dao = get_DaOffice_for_current_user(request)
    resp = dao.call('users.all',{'page':page})
    users = list()
    for user in resp:
        users.append(user._data)
    return response_json({
        'result' : True,
        'users': json.dumps(users)
    })

@login_required
def get_my_followers(request):
    dao = get_DaOffice_for_current_user(request)
    resp = dao.call('users.followers.%i'%dao.connection['userid'])
    users = list()
    for user in resp:
        users.append(user._data)
    return response_json({
        'result' : True,
        'users': json.dumps(users)
    })

@login_required
def get_followers(request, user_id=None):
    dao = get_DaOffice_for_current_user(request)
    resp = dao.call('users.followers.%i'%user_id)
    users = list()
    for user in resp:
        users.append(user._data)
    return response_json({
        'result' : True,
        'users': json.dumps(users)
    })