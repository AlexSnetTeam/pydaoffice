#
#
#
#
#
#
#
#

import types

# importing JSON
try:
    import simplejson as json # I mean simplejson is faster than json module
except ImportError:
    import json # but sometimes we don't have it

# DAO Exceptions
from .exceptions import *
from .methods import WrapResponse

class DaOffice(object):

    connection = {
        'url'   : None,
        'apiurl': None, # Getting by the first query /api/v1/network

        'login' : None,
        'pass'  : None,

        'token' : { 'token': None, 'expires':None, 'expires_in':None },
        'secret': None,

        'title' : None,
        'company': None,
        'gaids' : list(), # Google Analytics ID's

        'userid': None
    }

    _user = None

    last_response_object = None

    def __init__(self,
                 url=None,
                 login=None, password=None,
                 token=None, secret=None,

                 dontCallNetwork=False, # If you do not wannt to call network automatically
                 ):
        """
        Initializer with idiot-protection
        """
        self.connection['url'] = url if type(url) in types.StringTypes else None
        self.connection['url'] = self.connection['url'] if str(url).startswith('http://') or str(url).startswith('https://') else None
        if self.connection['url'] is None:
            raise DaoParameterMisplaced("url")

        self.connection['login'] = login if type(login) in types.StringTypes else None
        self.connection['pass'] = password if type(password) in types.StringTypes else None

        self.connection['token']['token'] = token if type(token) in types.StringTypes else None
        self.connection['secret'] = secret if type(secret) in types.StringTypes else None

        if not dontCallNetwork:
            self.network()

    #
    # Public methods
    #
    def time_to_token_expire(self):
        import time
        time_to_expire = int(time.time())-self.connection['token']['expires']
        if time_to_expire < 0:
            return abs(time_to_expire)
        else:
            return 0


    def call(self, method, params={}, headers={}):
        """
        call
        Generate wrapper for method.
        """
        method = self._normalize_method_name(method)
        wrapper =  WrapResponse(method=method, office=self, params=params, headers=headers,)
        self.last_response_object = wrapper

        return wrapper

    #
    # Method shortcuts
    #

    def network(self):
        self.call('network')()

    def get_profile(self):
        if not self.connection['token']['token']:
            raise DaoAuthorisationError
        return self.call('users.by_id.%i'%self.connection['userid'])._objects[0]

    #
    # Private methods
    #

    def _normalize_method_name(self, method):
        """
        Normalizing method name.
        Examples:
            /oauth/token -> oauth.token
            oauth..token -> oauth.token
            list('oauth', 'token') -> oauth.token

        """
        if type(method) in (types.ListType, types.TupleType,):
            method = '/'.join(method)
        if str(method).startswith('/'):
            method = method[1:]
        if '/' in method:
            method = method.replace('/','.')
        return method.strip('.')