import datetime

from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _, ugettext as __

from pydaoffice import DaOffice
from pydaoffice.signals import daoffice_user_registered

class DaoToken(models.Model):
    user                = models.ForeignKey(User, verbose_name=_('user'), unique=True)
    token               = models.CharField(max_length=255, verbose_name=_('token'), null=True, blank=True)
    expires             = models.DateTimeField(verbose_name=_('Expiration date'), null=True, blank=True)

    @classmethod
    def remove_expired(cls):
        cls.objects.filter(expires>=datetime.datetime.now()).delete()

    @property
    def valid(self):
        if self.expires:
            return self.expires < datetime.datetime.now()
        return False




class DaoProfile(models.Model):
    user                = models.ForeignKey(User, verbose_name=_('user'))

    dao_id              = models.PositiveIntegerField(verbose_name=_('DaOffice ID'), null=False, blank=False, unique=True)

    full_name           = models.CharField(max_length=255, verbose_name=_('user full name'), null=True, blank=True)
    #full_name_short     = models.CharField(max_length=100, verbose_name=_('User full name (short)'))

    login               = models.CharField(max_length=255, verbose_name=_('DaOffice login'), null=True, blank=True)

    external_id         = models.CharField(max_length=1000, verbose_name=_('external ID'), null=True, blank=True)

    created_at          = models.DateField(verbose_name=_('created at'), null=True, blank=True)
    is_active           = models.BooleanField(verbose_name=_('active'), default=True)

    web_url             = None
    url                 = None

    thumbnail_url       = None
    photo_large_url     = None


    birth_date          = models.DateField(verbose_name=_('birth date'), null=True, blank=True)

    main_alias          = None

    about_me            = models.TextField(verbose_name=_('about'), null=True, blank=True)
    education           = models.TextField(verbose_name=_('education'), null=True, blank=True)
    hobby               = models.TextField(verbose_name=_('hobby'), null=True, blank=True)
    work_experience     = models.TextField(verbose_name=_('work experience'), null=True, blank=True)

    subdivision_title   = models.CharField(max_length=100, verbose_name=_('subdivision title'), null=True, blank=True)
    position            = models.CharField(max_length=100, verbose_name=_('position'), null=True, blank=True)

    room_location       = models.CharField(max_length=255, verbose_name=_('room'), null=True, blank=True)

    contacts_email      = models.CharField(max_length=255, verbose_name=_('email'), null=True, blank=True)
    contacts_google_talk= models.CharField(max_length=255, verbose_name=_('GTalk'), null=True, blank=True)
    contacts_icq        = models.CharField(max_length=255, verbose_name=_('ICQ'), null=True, blank=True)
    contacts_phone_mobile= models.CharField(max_length=255, verbose_name=_('mobile phone'), null=True, blank=True)
    contacts_phone_work = models.CharField(max_length=255, verbose_name=_('work phone'), null=True, blank=True)
    contacts_skype      = models.CharField(max_length=255, verbose_name=_('skype'), null=True, blank=True)
    contacts_twitter    = models.CharField(max_length=255, verbose_name=_('twitter'), null=True, blank=True)

    @property
    def is_followed_by_current_user(self):
        return True

    def save(self, force_insert=False, force_update=False, using=None):
        super(DaoProfile, self).save(force_insert=force_insert, force_update=force_update, using=using)
        daoffice_user_registered.send(sender=self)