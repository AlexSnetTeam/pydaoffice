#
#
#
#
#
#
#
#

from __future__ import unicode_literals

import collections
import copy
import datetime
import decimal
import math
import warnings
from itertools import tee

from pydaoffice import exceptions

class NOT_PROVIDED:
    pass

EMPTY_VALUES = ( None, '', u'' )

# The values to use for "blank" in SelectFields. Will be appended to the start
# of most "choices" lists.
BLANK_CHOICE_DASH = [("", "---------")]
BLANK_CHOICE_NONE = [("", "None")]

class BaseField(object):
    default_validators = [] # Default set of validators
    default_error_messages = {
        'invalid_choice': 'Value %r is not a valid choice.',
        'null': 'This field cannot be null.',
        'blank': 'This field cannot be blank.',
        }

    def __init__(self, verbose_name=None, name=None, max_length=None, unique=False,
                 blank=False, null=False, default=NOT_PROVIDED, editable=True,
                 serialize=True, choices=None, help_text='',validators=[], error_messages=None):
        self.name = name
        self.verbose_name = verbose_name

        self.max_length, self._unique = max_length, unique
        self.blank, self.null = blank, null

        self.default = default
        self.editable = editable
        self.serialize = serialize

        self._choices = choices or []
        self.help_text = help_text

        self.validators = self.default_validators + validators

        messages = {}
        for c in reversed(self.__class__.__mro__):
            messages.update(getattr(c, 'default_error_messages', {}))
        messages.update(error_messages or {})
        self.error_messages = messages

    def _description(self):
        return 'Field of type: %(field_type)s' % {
            'field_type': self.__class__.__name__
        }
    description = property(_description)

    __hash__ = object.__hash__

    def __deepcopy__(self, memodict):
        # We don't have to deepcopy very much here, since most things are not
        # intended to be altered after initial creation.
        obj = copy.copy(self)
        memodict[id(self)] = obj
        return obj

    def to_python(self, value):
        """
        Converts the input value into the expected Python data type, raising
        django.core.exceptions.ValidationError if the data can't be converted.
        Returns the converted value. Subclasses should override this.
        """
        return value

    def run_validators(self, value):
        if value in EMPTY_VALUES:
            return

        errors = []
        for v in self.validators:
            try:
                v(value)
            except exceptions.DaoValidationError as e:
                if hasattr(e, 'code') and e.code in self.error_messages:
                    message = self.error_messages[e.code]
                    if e.extra:
                        message = message % str(e.extra)
                    errors.append(message)
                else:
                    errors.extend(e.messages)
        if errors:
            raise exceptions.DaoValidationError(errors)

    def get_attname(self):
        return self.name

    def get_internal_type(self):
        return self.__class__.__name__

    def has_default(self):
        """
        Returns a boolean of whether this field has a default value.
        """
        return self.default is not NOT_PROVIDED

    def get_default(self):
        """
        Returns the default value for this field.
        """
        if self.has_default():
            if callable(self.default):
                return self.default()
            return str(self.default)
        if self.null:
            return None
        return ""

    def get_choices(self, include_blank=True, blank_choice=BLANK_CHOICE_DASH):
        """Returns choices with a default blank choices included, for use
        as SelectField choices for this field."""
        first_choice = include_blank and blank_choice or []
        if self.choices:
            return first_choice + list(self.choices)

        return first_choice

    def _get_choices(self):
        if isinstance(self._choices, collections.Iterator):
            choices, self._choices = tee(self._choices)
            return choices
        else:
            return self._choices
    choices = property(_get_choices)

    def _get_flatchoices(self):
        """Flattened version of choices tuple."""
        flat = []
        for choice, value in self.choices:
            if isinstance(value, (list, tuple)):
                flat.extend(value)
            else:
                flat.append((choice,value))
        return flat
    flatchoices = property(_get_flatchoices)

    def value_from_object(self, obj):
        """
        Returns the value of this field in the given model instance.
        """
        return getattr(obj, self.name)

    def __repr__(self):
        """
        Displays the module, class and name of the field.
        """
        path = '%s.%s' % (self.__class__.__module__, self.__class__.__name__)
        name = getattr(self, 'name', None)
        if name is not None:
            return '<%s: %s>' % (path, name)
        return '<%s>' % path

class TextField(BaseField):
    pass

class CharField(BaseField):
    pass

class SlugField(CharField):
    pass

class DateField(BaseField):
    pass

class BoolField(BaseField):
    pass

class EmailField(CharField):
    pass

class UrlField(CharField):
    pass

class FileField(UrlField):
    pass

class ImageField(FileField):
    pass

class IntegerField(BaseField):
    pass

class FloatField(IntegerField):
    pass