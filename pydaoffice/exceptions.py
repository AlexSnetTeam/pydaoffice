#
#
#
#
#
#
#
#

class DaoExceptionBase(Exception):
    messages = list()
    def __init__(self, message='', extra=None):
        self.extra = extra
        super(DaoExceptionBase, self).__init__(message)

class DaoValidationError(DaoExceptionBase):
    pass

class DaoParameterMisplaced(DaoExceptionBase):
    pass

class DaoWrongResponseObjectError(DaoExceptionBase):
    pass

class DaoWrongNoResponseObjectError(DaoExceptionBase):
    pass

class DaoWrongOfficeObjectError(DaoExceptionBase):
    pass

class DaoResponseError(DaoExceptionBase):
    pass

class DaoWrongResponseStatus(DaoExceptionBase):
    pass

class DaoHttpError(DaoExceptionBase):
    pass

class DaoAuthorizationError(DaoExceptionBase):
    pass

class DaoAuthorisationError(DaoAuthorizationError):
    pass
