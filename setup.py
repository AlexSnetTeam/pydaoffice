#!/usr/bin/env python

import os, sys, re
from distutils.core import setup
from setuptools import setup, find_packages

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

def parse_requirements(file_name):
    requirements = []
    for line in open(file_name, 'r').read().split('\n'):
        if re.match(r'(\s*#)|(\s*$)', line):
            continue
        if re.match(r'\s*-e\s+', line):
            requirements.append(re.sub(r'\s*-e\s+.*#egg=(.*)$', r'\1', line))
        elif re.match(r'\s*-f\s+', line):
            pass
        else:
            requirements.append(line)

    return requirements

def parse_dependency_links(file_name):
    dependency_links = []
    for line in open(file_name, 'r').read().split('\n'):
        if re.match(r'\s*-[ef]\s+', line):
            dependency_links.append(re.sub(r'\s*-[ef]\s+', '', line))

    return dependency_links

setup(
    name='pydaoffice',
    version='12.9.26.1',

    install_requires = ['requests',],

    packages=['pydaoffice'],
    data_files=[('',['license.txt','README.md'])],

    url='https://bitbucket.org/AlexSnet/pydaoffice',
    download_url='https://bitbucket.org/AlexSnet/pydaoffice/get/master.tar.gz',

    license='LGPL',

    author='Alexey Grechishkin',
    author_email='me@alexsnet.ru',

    description='DaOffice (http://daoffice.ru/) python API client',
    long_description=read('README.md'),

    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "Programming Language :: Python :: 2.6",
        "Programming Language :: Python :: 2.7",
        "Operating System :: OS Independent",
        "Topic :: Utilities",
        "License :: OSI Approved :: BSD License",
        ],
)
